# Phobos Plugin SDK

This repository is a Go library that enables users to write custom [Phobos](https://phobos.martian-cloud.io/) plugins.

Plugins in Phobos are separate binaries which communicate with the Phobos application; the plugin communicates using
gRPC.

## Simple Plugin Overview

To initialize the plugin SDK you use the `Main` function in the `sdk` package and register a plugin which provides
callbacks for Phobos during the various parts of the lifecycle.

```go
package main

import (
  sdk "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk"
)

func main() {

  sdk.Main(
    sdk.WithPluginDescription("This is an example plugin"),
    sdk.WithPipelinePlugin(
      &internal.Plugin{},
    ),
  )

}
```

The pipeline plugin struct must implement the following interface:

```go
type Plugin interface {
  Metadata() *Metadata
  Config() (any, error)
  ValidateConfig(config any) error
  PluginData(logger hclog.Logger) (any, error)
  GetActions() []func() Action
}
```

### Installing Dependencies

To automate installing the required Golang packages needed to build Phobos locally, run `make tools`.

### Regenerating protobufs

Install docker and run `make gen`

## Security

If you've discovered a security vulnerability in the Phobos API, please let us know by creating a **confidential** issue in this project.

## Statement of support

Please submit any bugs or feature requests for Phobos. Of course, MR's are even better. :)

## License

Phobos API is distributed under [Mozilla Public License v2.0](https://www.mozilla.org/en-US/MPL/2.0/).
