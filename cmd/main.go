// Package main provides the entrypoint for the CLI tool.
package main

import (
	"os"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal/cmd"
)

// version is passed in via ldflags at build time
var version = "dev"

func main() {
	os.Exit(cmd.Run(version))
}
