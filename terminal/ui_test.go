package terminal

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNamedValues(t *testing.T) {
	require := require.New(t)

	var buf bytes.Buffer
	ui := nonInteractiveUI{w: &buf}
	ui.NamedValues([]NamedValue{
		{Name: "hello", Value: "a"},
	},
	)

	expected := "  hello: a\n\n"

	require.Equal(expected, buf.String())
}

func TestNamedValues_server(t *testing.T) {
	require := require.New(t)

	var buf bytes.Buffer
	ui := nonInteractiveUI{w: &buf}
	ui.Output("Server configuration:", WithHeaderStyle())
	ui.NamedValues([]NamedValue{
		{Name: "DB Path", Value: "data.db"},
		{Name: "gRPC Address", Value: "127.0.0.1:1234"},
	},
	)

	expected := "\x1b[1m\n» Server configuration:\x1b[22m\n       DB Path: data.db\n  gRPC Address: 127.0.0.1:1234\n\n"

	require.Equal(expected, buf.String())
}
