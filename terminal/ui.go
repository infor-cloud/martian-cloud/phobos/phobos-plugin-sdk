package terminal

import (
	"fmt"
	"io"

	"github.com/fatih/color"
)

func init() {
	// NoColor must be explicitly set to false because the color library
	// will not detect a tty
	color.NoColor = false
}

// NamedValue provides a nicely formatted key: value output
type NamedValue struct {
	Value interface{}
	Name  string
}

// UI is the primary interface for interacting with a user via the CLI.
//
// Some of the methods on this interface return values that have a lifetime
// such as Status and StepGroup. While these are still active (haven't called
// the close or equivalent method on these values), no other method on the
// UI should be called.
type UI interface {
	// Output outputs a message directly to the terminal. The remaining
	// arguments should be interpolations for the format string. After the
	// interpolations you may add Options.
	Output(string, ...interface{})

	// Output data as a table of data. Each entry is a row which will be output
	// with the columns lined up nicely.
	NamedValues([]NamedValue, ...Option)

	// OutputWriters returns stdout and stderr writers. These are usually
	// but not always TTYs. This is useful for subprocesses, network requests,
	// etc. Note that writing to these is not thread-safe by default so
	// you must take care that there is only ever one writer.
	OutputWriter() (io.WriteCloser, error)

	// Table outputs the information formatted into a Table structure.
	Table(*Table, ...Option)
}

// Interpret decomposes the msg and arguments into the message, style, and writer
func Interpret(msg string, raw ...interface{}) (string, string) {
	// Build our args and options
	var args []interface{}
	var opts []Option
	for _, r := range raw {
		if opt, ok := r.(Option); ok {
			opts = append(opts, opt)
		} else {
			args = append(args, r)
		}
	}

	// Build our message
	msg = fmt.Sprintf(msg, args...)

	// Build our config and set our options
	cfg := &config{}
	for _, opt := range opts {
		opt(cfg)
	}

	return msg, cfg.Style
}

// Style constants
const (
	HeaderStyle      = "header"
	ErrorStyle       = "error"
	WarningStyle     = "warning"
	InfoStyle        = "info"
	SuccessStyle     = "success"
	SuccessBoldStyle = "success-bold"
)

type config struct {
	// The style the output should take on
	Style string
}

// Option controls output styling.
type Option func(*config)

// WithHeaderStyle styles the output like a header denoting a new section
// of execution. This should only be used with single-line output. Multi-line
// output will not look correct.
func WithHeaderStyle() Option {
	return func(c *config) {
		c.Style = HeaderStyle
	}
}

// WithInfoStyle styles the output like it's formatted information.
func WithInfoStyle() Option {
	return func(c *config) {
		c.Style = InfoStyle
	}
}

// WithErrorStyle styles the output as an error message.
func WithErrorStyle() Option {
	return func(c *config) {
		c.Style = ErrorStyle
	}
}

// WithWarningStyle styles the output as an error message.
func WithWarningStyle() Option {
	return func(c *config) {
		c.Style = WarningStyle
	}
}

// WithSuccessStyle styles the output as a success message.
func WithSuccessStyle() Option {
	return func(c *config) {
		c.Style = SuccessStyle
	}
}

// WithSuccessBoldStyle styles the output as a success message.
func WithSuccessBoldStyle() Option {
	return func(c *config) {
		c.Style = SuccessBoldStyle
	}
}

// WithStyle adds a style option
func WithStyle(style string) Option {
	return func(c *config) {
		c.Style = style
	}
}

// Color constants
var (
	colorHeader      = color.New(color.Bold)
	colorInfo        = color.New(color.FgHiBlue)
	colorError       = color.New(color.FgRed)
	colorErrorBold   = color.New(color.FgRed, color.Bold)
	colorSuccess     = color.New(color.FgGreen)
	colorSuccessBold = color.New(color.FgGreen, color.Bold)
	colorWarning     = color.New(color.FgYellow)
	colorWarningBold = color.New(color.FgYellow, color.Bold)
)

var colorMap = map[string]*color.Color{
	HeaderStyle:      colorHeader,
	InfoStyle:        colorInfo,
	ErrorStyle:       colorError,
	WarningStyle:     colorWarning,
	SuccessStyle:     colorSuccess,
	SuccessBoldStyle: colorSuccessBold,
}
