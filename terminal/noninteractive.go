package terminal

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strings"
	"sync"
	"text/tabwriter"

	"github.com/olekukonko/tablewriter"
)

type outputWriter struct {
	io.Writer
}

func (ow *outputWriter) Close() error {
	// This is a noop
	return nil
}

type nonInteractiveUI struct {
	w  io.Writer
	mu sync.Mutex
}

// NonInteractiveUI creates a new instance of a UI implementation that is non-interactive
func NonInteractiveUI(_ context.Context, w io.Writer) UI {
	result := &nonInteractiveUI{w: w}
	return result
}

// Output implements UI
func (ui *nonInteractiveUI) Output(msg string, raw ...interface{}) {
	ui.mu.Lock()
	defer ui.mu.Unlock()
	msg, style := Interpret(msg, raw...)

	switch style {
	case HeaderStyle:
		msg = colorHeader.Sprintf("\n» " + msg)
	case ErrorStyle:
		lines := strings.Split(msg, "\n")
		if len(lines) > 0 {
			lines[0] = colorErrorBold.Sprintf("ERROR: %s", lines[0])
			for i, line := range lines[1:] {
				lines[i+1] = colorError.Sprint("  " + line)
			}
		}
		msg = strings.Join(lines, "\n")
	case WarningStyle:
		lines := strings.Split(msg, "\n")
		if len(lines) > 0 {
			lines[0] = colorWarningBold.Sprintf("WARNING: %s", lines[0])
			for i, line := range lines[1:] {
				lines[i+1] = colorWarning.Sprint("  " + line)
			}
		}
		msg = strings.Join(lines, "\n")

	case InfoStyle, SuccessStyle, SuccessBoldStyle:
		c := colorMap[style]

		lines := strings.Split(msg, "\n")
		for i, line := range lines {
			lines[i] = c.Sprint(line)
		}

		msg = strings.Join(lines, "\n")
	}

	fmt.Fprintln(ui.w, msg)
}

// NamedValues implements UI
func (ui *nonInteractiveUI) NamedValues(rows []NamedValue, opts ...Option) {
	ui.mu.Lock()
	defer ui.mu.Unlock()

	cfg := &config{}
	for _, opt := range opts {
		opt(cfg)
	}

	var buf bytes.Buffer
	tr := tabwriter.NewWriter(&buf, 1, 8, 0, ' ', tabwriter.AlignRight)
	for _, row := range rows {
		switch v := row.Value.(type) {
		case int, uint, int8, uint8, int16, uint16, int32, uint32, int64, uint64:
			fmt.Fprintf(tr, "  %s: \t%d\n", row.Name, row.Value)
		case float32, float64:
			fmt.Fprintf(tr, "  %s: \t%f\n", row.Name, row.Value)
		case bool:
			fmt.Fprintf(tr, "  %s: \t%v\n", row.Name, row.Value)
		case string:
			if v == "" {
				continue
			}
			fmt.Fprintf(tr, "  %s: \t%s\n", row.Name, row.Value)
		default:
			fmt.Fprintf(tr, "  %s: \t%s\n", row.Name, row.Value)
		}
	}
	tr.Flush()

	fmt.Fprintln(ui.w, buf.String())
}

// OutputWriters implements UI
func (ui *nonInteractiveUI) OutputWriter() (io.WriteCloser, error) {
	return &outputWriter{Writer: ui.w}, nil
}

// Table implements UI
func (ui *nonInteractiveUI) Table(tbl *Table, opts ...Option) {
	ui.mu.Lock()
	defer ui.mu.Unlock()

	// Build our config and set our options
	cfg := &config{}
	for _, opt := range opts {
		opt(cfg)
	}

	table := tablewriter.NewWriter(ui.w)
	table.SetHeader(tbl.Headers)
	table.SetBorder(false)
	table.SetAutoWrapText(true)

	if cfg.Style == "Simple" {
		// Format the table without borders, simple output

		table.SetAutoFormatHeaders(true)
		table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
		table.SetAlignment(tablewriter.ALIGN_LEFT)
		table.SetCenterSeparator("")
		table.SetColumnSeparator("")
		table.SetRowSeparator("")
		table.SetHeaderLine(false)
		table.SetTablePadding("\t") // pad with tabs
		table.SetNoWhiteSpace(true)
	}

	for _, row := range tbl.Rows {
		colors := make([]tablewriter.Colors, len(row))
		entries := make([]string, len(row))

		for i, ent := range row {
			entries[i] = ent.Value

			color, ok := colorMapping[ent.Color]
			if ok {
				colors[i] = tablewriter.Colors{color}
			}
		}

		table.Rich(entries, colors)
	}

	table.Render()
}
