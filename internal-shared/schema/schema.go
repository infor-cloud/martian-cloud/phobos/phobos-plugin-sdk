// Package schema defines the schema format for plugins
package schema

import "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"

// PluginSchema defines the schema for a plugin
type PluginSchema struct {
	Name             string                          `json:"name"`
	Description      string                          `json:"description"`
	PipelineSchema   *component.PipelinePluginSchema `json:"pipeline"`
	ProtocolVersions []string                        `json:"protocol_versions"`
	FormatVersion    string                          `json:"format_version"`
}
