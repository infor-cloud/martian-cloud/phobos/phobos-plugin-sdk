// Package pluginclient is used to initialize a new plugin instance
package pluginclient

import (
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"

	internalplugin "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal/plugin"
)

// ClientConfig returns the base client config to use when connecting
// to a plugin. This sets the handshake config, protocols, etc. Manually
// override any values you want to set.
func ClientConfig(log hclog.Logger) *plugin.ClientConfig {
	return &plugin.ClientConfig{
		HandshakeConfig: internalplugin.Handshake,
		VersionedPlugins: internalplugin.Plugins(
			internalplugin.WithLogger(log),
		),
		AllowedProtocols: []plugin.Protocol{plugin.ProtocolGRPC},

		// We always set managed to true just in case we don't properly
		// call Kill so that CleanupClients gets it. If we do properly call
		// Kill, then it is a no-op to call it again so this is safe.
		Managed: true,

		// This is super important. There appears to be a bug with AutoMTLS
		// when using GRPCBroker and listening from the _client_ side. The
		// TLS fails to negotiate. For now we just disable this but we should
		// look into fixing that later.
		AutoMTLS: false,
	}
}
