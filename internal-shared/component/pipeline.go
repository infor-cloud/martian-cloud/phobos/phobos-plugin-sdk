package component

import (
	"context"

	"github.com/hashicorp/hcl/v2"
	"github.com/zclconf/go-cty/cty"
)

// PipelinePluginActionInput is the input for executing an action
type PipelinePluginActionInput struct {
	InputData  hcl.Body
	ActionName string
}

// PipelinePluginActionResponse is the response after execution an action
type PipelinePluginActionResponse interface {
	Outputs() map[string]cty.Value
}

// PipelinePluginActionSchema represents a single action in a pipeline plugin schema
type PipelinePluginActionSchema struct {
	Input       *SchemaBlock                `json:"input"`
	Outputs     map[string]*SchemaAttribute `json:"outputs,omitempty"`
	Name        string                      `json:"name"`
	Description string                      `json:"description"`
	Group       string                      `json:"group"`
	Deprecated  bool                        `json:"deprecated"`
}

// PipelinePluginSchema represents the schema of a pipeline plugin
type PipelinePluginSchema struct {
	Config  *SchemaBlock                           `json:"config"`
	Actions map[string]*PipelinePluginActionSchema `json:"actions"`
}

// PipelinePlugin represents a pipeline plugin
type PipelinePlugin interface {
	ActionInput(ctx context.Context, actionName string) (interface{}, error)
	ActionOutput(ctx context.Context, actionName string) (interface{}, error)
	ExecuteAction(ctx context.Context, input *PipelinePluginActionInput, evalCtx *hcl.EvalContext) interface{}
	Shutdown(ctx context.Context) error
	Config(ctx context.Context) (interface{}, error)
	ConfigSet(ctx context.Context, config interface{}) error
	Schema(ctx context.Context) (*PipelinePluginSchema, error)
}
