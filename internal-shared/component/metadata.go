package component

import "context"

// PluginMetadata is the metadata for a plugin
type PluginMetadata struct {
	Description string `json:"description"`
}

// MetadataPlugin is a plugin that provides metadata
type MetadataPlugin interface {
	PluginMetadata(ctx context.Context) (*PluginMetadata, error)
}
