// Package component exposes the component interfaces
package component

import (
	"github.com/zclconf/go-cty/cty"
)

//go:generate stringer -type=Type -linecomment
//go:generate mockery --all --case="underscore"

// Type is an enum of all the types of components supported.
// This isn't used directly in this package but is used by other packages
// to reference the component types.
type Type uint

// Type enum constants
const (
	InvalidType  Type = iota // Invalid
	MetadataType             // Metadata
	PipelineType             // Pipeline
	maxType
)

// TypeMap is a mapping of Type to the nil pointer to the interface of that
// type. This can be used with libraries such as mapper.
var TypeMap = map[Type]interface{}{
	PipelineType: (*PipelinePlugin)(nil),
	MetadataType: (*MetadataPlugin)(nil),
}

// SchemaAttribute represents a single attribute in a schema
type SchemaAttribute struct {
	Name             string                      `json:"name"`
	Description      string                      `json:"description"`
	Type             cty.Type                    `json:"type"`
	Deprecated       bool                        `json:"deprecated"`
	NestedAttributes map[string]*SchemaAttribute `json:"nested_attributes"`
	Required         *bool                       `json:"required,omitempty"`
}

// SchemaBlock represents a single HCL block in a schema
type SchemaBlock struct {
	Attributes map[string]*SchemaAttribute `json:"attributes"`
	BlockSpecs map[string]*SchemaBlockSpec `json:"block_specs"`
}

// SchemaBlockSpec represents an HCL block field
type SchemaBlockSpec struct {
	Name        string       `json:"name"`
	Type        cty.Type     `json:"type"`
	Description string       `json:"description"`
	Deprecated  bool         `json:"deprecated"`
	Required    bool         `json:"required"`
	Block       *SchemaBlock `json:"block"`
}
