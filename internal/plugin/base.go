package plugin

import (
	"context"

	"github.com/hashicorp/go-argmapper"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal/pluginargs"
)

// base contains shared logic for all plugins. This should be embedded
// in every plugin implementation.
type base struct {
	Broker  *plugin.GRPCBroker
	Logger  hclog.Logger
	Mappers []*argmapper.Func
}

// internal returns a new pluginargs.Internal that can be used with
// dynamic calls. The Internal structure is an internal-only argument
// that is used to perform cleanup.
func (b *base) internal(grpcContext context.Context) *pluginargs.Internal {
	return &pluginargs.Internal{
		Broker:      b.Broker,
		Mappers:     b.Mappers,
		Cleanup:     &pluginargs.Cleanup{},
		GRPCContext: grpcContext,
	}
}
