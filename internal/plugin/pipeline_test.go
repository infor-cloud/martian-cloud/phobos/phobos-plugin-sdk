package plugin

import (
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/zclconf/go-cty/cty"
)

func TestOutputDataFromStruct(t *testing.T) {
	cases := []struct {
		Name   string
		Input  interface{}
		Output map[string]cty.Value
	}{
		{
			"all-in-one",
			struct {
				Name      string
				Port      int
				ImageName string
				Tags      map[string]string
				Labels    []string
			}{
				Name:      "Hello",
				Port:      80,
				ImageName: "foo",
				Tags:      map[string]string{"env": "test"},
				Labels:    []string{"foo", "bar"},
			},
			map[string]cty.Value{
				"name":       cty.StringVal("Hello"),
				"port":       cty.NumberIntVal(80),
				"image_name": cty.StringVal("foo"),
				"tags": cty.MapVal(map[string]cty.Value{
					"env": cty.StringVal("test"),
				}),
				"labels": cty.ListVal([]cty.Value{cty.StringVal("foo"), cty.StringVal("bar")}),
			},
		},

		{
			"pointer",
			&struct {
				Name string
				Port int
			}{
				Name: "Hello",
				Port: 80,
			},
			map[string]cty.Value{
				"name": cty.StringVal("Hello"),
				"port": cty.NumberIntVal(80),
			},
		},

		{
			"protobuf fields",
			&struct {
				Name     string
				XXXHello string
			}{
				Name:     "Hello",
				XXXHello: "hi",
			},
			map[string]cty.Value{
				"name": cty.StringVal("Hello"),
			},
		},
	}

	for _, test := range cases {
		t.Run(test.Name, func(t *testing.T) {
			require := require.New(t)

			v, err := convertStructToOutputMap(test.Input)
			if err != nil {
				t.Fatal(err)
			}

			require.Equal(test.Output, v)
		})
	}
}
