package plugin

import (
	"context"
	"fmt"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	empty "google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/proto/gen"
)

// MetadataPlugin implements plugin.Plugin (specifically GRPCPlugin)
type MetadataPlugin struct {
	plugin.NetRPCUnsupportedPlugin
	Logger      hclog.Logger
	Description string
}

// GRPCServer returns the GRPC server
func (p *MetadataPlugin) GRPCServer(broker *plugin.GRPCBroker, s *grpc.Server) error {
	base := &base{
		Logger: p.Logger,
		Broker: broker,
	}

	pb.RegisterMetadataServiceServer(s, &metadataServer{
		base:        base,
		description: p.Description,
	})
	return nil
}

// GRPCClient returns the GRPC client
func (p *MetadataPlugin) GRPCClient(
	_ context.Context,
	_ *plugin.GRPCBroker,
	c *grpc.ClientConn,
) (interface{}, error) {
	client := &metadataClient{
		client: pb.NewMetadataServiceClient(c),
		logger: p.Logger,
	}

	return client, nil
}

type metadataClient struct {
	client pb.MetadataServiceClient
	logger hclog.Logger
}

func (c *metadataClient) PluginMetadata(ctx context.Context) (*component.PluginMetadata, error) {
	// Get the metadata
	resp, err := c.client.PluginMetadata(ctx, &empty.Empty{})
	if err != nil {
		return nil, fmt.Errorf("failed to get metadata: %w", err)
	}

	return &component.PluginMetadata{
		Description: resp.Description,
	}, nil
}

// metadataServer is a gRPC server that the client talks to and calls a
// real implementation of the component.
type metadataServer struct {
	*base

	pb.UnsafeMetadataServiceServer // to avoid having to copy stubs into here for authServer

	description string
}

func (s *metadataServer) PluginMetadata(
	_ context.Context,
	_ *emptypb.Empty,
) (*pb.Metadata_Plugin, error) {
	return &pb.Metadata_Plugin{
		Description: s.description,
	}, nil
}

var (
	_ plugin.Plugin            = (*MetadataPlugin)(nil)
	_ plugin.GRPCPlugin        = (*MetadataPlugin)(nil)
	_ pb.MetadataServiceServer = (*metadataServer)(nil)
	_ component.MetadataPlugin = (*metadataClient)(nil)
)
