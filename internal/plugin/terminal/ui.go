// Package terminal provides the GRPC terminal implementations
package terminal

import (
	"context"
	"fmt"
	"io"
	"sync"

	"github.com/hashicorp/go-argmapper"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	"google.golang.org/grpc"

	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/proto/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
)

// UIPlugin implements plugin.Plugin (specifically GRPCPlugin) for
// the terminal.UI interface.
type UIPlugin struct {
	plugin.NetRPCUnsupportedPlugin

	Impl    terminal.UI       // Impl is the concrete implementation
	Mappers []*argmapper.Func // Mappers
	Logger  hclog.Logger      // Logger
}

// GRPCServer returns the GRPC server
func (p *UIPlugin) GRPCServer(_ *plugin.GRPCBroker, s *grpc.Server) error {
	pb.RegisterTerminalUIServiceServer(s, &uiServer{
		Impl:    p.Impl,
		Mappers: p.Mappers,
		Logger:  p.Logger,
	})
	return nil
}

// GRPCClient returns the GRPC client
func (p *UIPlugin) GRPCClient(
	ctx context.Context,
	_ *plugin.GRPCBroker,
	c *grpc.ClientConn,
) (interface{}, error) {
	client := pb.NewTerminalUIServiceClient(c)

	evstream, err := client.Events(ctx)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithCancel(ctx)

	return &uiBridge{
		ctx:    ctx,
		cancel: cancel,
		evc:    evstream,
	}, nil
}

// uiServer is a gRPC server that the client talks to and calls a
// real implementation of the component.
type uiServer struct {
	pb.UnimplementedTerminalUIServiceServer

	Impl    terminal.UI
	Mappers []*argmapper.Func
	Logger  hclog.Logger
}

func (s *uiServer) Events(stream pb.TerminalUIService_EventsServer) error {
	var (
		stdout io.Writer
	)

	for {
		ev, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				return nil
			}

			return err
		}

		switch ev := ev.Event.(type) {
		case *pb.TerminalUI_Event_Line_:
			s.Impl.Output(ev.Line.Msg, terminal.WithStyle(ev.Line.Style))
		case *pb.TerminalUI_Event_NamedValues_:
			var values []terminal.NamedValue

			for _, nv := range ev.NamedValues.Values {
				values = append(values, terminal.NamedValue{
					Name:  nv.Name,
					Value: nv.Value,
				})
			}

			s.Impl.NamedValues(values)
		case *pb.TerminalUI_Event_Raw_:
			if stdout == nil {
				stdout, err = s.Impl.OutputWriter()
				if err != nil {
					return err
				}
			}

			stdout.Write(ev.Raw.Data)
		case *pb.TerminalUI_Event_Table_:
			tbl := terminal.NewTable(ev.Table.Headers...)

			for _, row := range ev.Table.Rows {
				var trow []terminal.TableEntry

				for _, ent := range row.Entries {
					trow = append(trow, terminal.TableEntry{
						Value: ent.Value,
						Color: ent.Color,
					})
				}

				tbl.Rows = append(tbl.Rows, trow)
			}

			s.Impl.Table(tbl)
		default:
			s.Logger.Error("Unknown terminal event seen", "type", hclog.Fmt("%T", ev))
		}
	}
}

type uiBridge struct {
	ctx    context.Context
	evc    pb.TerminalUIService_EventsClient
	cancel func()
	mu     sync.Mutex
}

func (u *uiBridge) Close() error {
	u.mu.Lock()
	defer u.mu.Unlock()

	err := u.evc.CloseSend()

	// The remote side never sends anything back to us, so this will just wait
	// until the remote side has seen our closure and the stream has been
	// closed. We don't actually care if there is an error here, just that
	// we did wait.
	u.evc.Recv()

	u.evc = nil
	u.cancel()

	return err
}

// Output outputs a message directly to the terminal. The remaining
// arguments should be interpolations for the format string. After the
// interpolations you may add Options.
func (u *uiBridge) Output(msg string, raw ...interface{}) {
	msg, style := terminal.Interpret(msg, raw...)

	ev := &pb.TerminalUI_Event{
		Event: &pb.TerminalUI_Event_Line_{
			Line: &pb.TerminalUI_Event_Line{
				Msg:   msg,
				Style: style,
			},
		},
	}

	u.mu.Lock()
	defer u.mu.Unlock()

	if u.evc == nil {
		return
	}

	err := u.evc.Send(ev)
	if err != nil {
		// This should never return an error, but we panic because this function does not return an error
		panic(err)
	}
}

// Output data as a table of data. Each entry is a row which will be output
// with the columns lined up nicely.
func (u *uiBridge) NamedValues(tvalues []terminal.NamedValue, _ ...terminal.Option) {
	var values []*pb.TerminalUI_Event_NamedValue

	for _, nv := range tvalues {
		values = append(values, &pb.TerminalUI_Event_NamedValue{
			Name:  nv.Name,
			Value: fmt.Sprintf("%s", nv.Value),
		})
	}

	u.mu.Lock()
	defer u.mu.Unlock()

	if u.evc == nil {
		return
	}

	u.evc.Send(&pb.TerminalUI_Event{
		Event: &pb.TerminalUI_Event_NamedValues_{
			NamedValues: &pb.TerminalUI_Event_NamedValues{
				Values: values,
			},
		},
	})
}

// OutputWriters returns stdout writer. These are usually
// but not always TTYs. This is useful for subprocesses, network requests,
// etc. Note that writing to these is not thread-safe by default so
// you must take care that there is only ever one writer.
func (u *uiBridge) OutputWriter() (io.WriteCloser, error) {
	// Must use io.Pipe here instead of os.Pipe to ensure that pipe
	// will block to provide flow control
	dr, dw := io.Pipe()

	go u.sendData(dr)

	go func() {
		<-u.ctx.Done()
		dw.Close()
	}()

	return dw, nil
}

func (u *uiBridge) sendData(r io.ReadCloser) {
	defer r.Close()

	u.mu.Lock()
	defer u.mu.Unlock()

	buf := make([]byte, 1024)

	for {
		n, err := r.Read(buf)
		if err != nil {
			return
		}

		data := buf[:n]

		ev := &pb.TerminalUI_Event{
			Event: &pb.TerminalUI_Event_Raw_{
				Raw: &pb.TerminalUI_Event_Raw{
					Data: data,
				},
			},
		}

		if u.evc == nil {
			return
		}

		u.evc.Send(ev)
	}
}

func (u *uiBridge) Table(tbl *terminal.Table, _ ...terminal.Option) {
	ptbl := pb.TerminalUI_Event_Table{
		Headers: tbl.Headers,
		Rows:    []*pb.TerminalUI_Event_TableRow{},
	}

	ptbl.Headers = tbl.Headers

	for _, row := range tbl.Rows {
		entries := []*pb.TerminalUI_Event_TableEntry{}

		for _, ent := range row {
			entries = append(entries, &pb.TerminalUI_Event_TableEntry{
				Value: ent.Value,
				Color: ent.Color,
			})
		}

		ptbl.Rows = append(ptbl.Rows, &pb.TerminalUI_Event_TableRow{
			Entries: entries,
		})
	}

	u.mu.Lock()
	defer u.mu.Unlock()

	if u.evc == nil {
		return
	}

	u.evc.Send(&pb.TerminalUI_Event{
		Event: &pb.TerminalUI_Event_Table_{
			Table: &ptbl,
		},
	})
}

var (
	_ plugin.Plugin              = (*UIPlugin)(nil)
	_ plugin.GRPCPlugin          = (*UIPlugin)(nil)
	_ pb.TerminalUIServiceServer = (*uiServer)(nil)
	_ terminal.UI                = (*uiBridge)(nil)
)
