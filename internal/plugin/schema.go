package plugin

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/iancoleman/strcase"
	"github.com/zclconf/go-cty/cty"
	"github.com/zclconf/go-cty/cty/gocty"
	ctyjson "github.com/zclconf/go-cty/cty/json"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/proto/gen"
)

func schemaAttributeFromProto(f *pb.Plugin_SchemaAttribute) (*component.SchemaAttribute, error) {
	nested := map[string]*component.SchemaAttribute{}
	for _, f := range f.NestedAttributes {
		attr, err := schemaAttributeFromProto(f)
		if err != nil {
			return nil, err
		}
		nested[f.Name] = attr
	}

	// Unmarshal cty type
	ty, err := ctyjson.UnmarshalType(f.Type)
	if err != nil {
		return nil, err
	}

	return &component.SchemaAttribute{
		Name:             f.Name,
		Description:      f.Description,
		Type:             ty,
		Required:         f.Required,
		Deprecated:       f.Deprecated,
		NestedAttributes: nested,
	}, nil
}

func schemaBlockFromProto(b *pb.Plugin_SchemaBlock) (*component.SchemaBlock, error) {
	attributes := map[string]*component.SchemaAttribute{}

	for name, a := range b.Attributes {
		attr, err := schemaAttributeFromProto(a)
		if err != nil {
			return nil, err
		}
		attributes[name] = attr
	}

	nested := map[string]*component.SchemaBlockSpec{}
	for name, b := range b.BlockSpecs {
		block, err := schemaBlockFieldFromProto(b)
		if err != nil {
			return nil, err
		}
		nested[name] = block
	}

	return &component.SchemaBlock{
		Attributes: attributes,
		BlockSpecs: nested,
	}, nil
}

func schemaBlockFieldFromProto(b *pb.Plugin_SchemaBlockSpec) (*component.SchemaBlockSpec, error) {
	// Unmarshal cty type
	ty, err := ctyjson.UnmarshalType(b.Type)
	if err != nil {
		return nil, err
	}

	block, err := schemaBlockFromProto(b.Block)
	if err != nil {
		return nil, err
	}

	return &component.SchemaBlockSpec{
		Name:        b.Name,
		Deprecated:  b.Deprecated,
		Type:        ty,
		Required:    b.Required,
		Description: b.Description,
		Block:       block,
	}, nil
}

func schemaAttributeFromField(f reflect.StructField) (*pb.Plugin_SchemaAttribute, error) {
	// First attempt to get the name from the hcl tag
	name, required, _ := parseHclTag(f.Tag)
	if name == "" {
		// Nested types use cty tag
		name = f.Tag.Get("cty")
	}

	v := reflect.New(f.Type)
	// Get implied cty type
	ty, err := gocty.ImpliedType(v.Interface())
	if err != nil {
		return nil, fmt.Errorf("failed to get implied cty type for field %s: %w", f.Name, err)
	}
	typeBuf, err := ty.MarshalJSON()
	if err != nil {
		return nil, fmt.Errorf("failed to marshal cty type for field %s: %w", name, err)
	}

	// Unwind pointers and slices
	t := f.Type
	for t.Kind() == reflect.Ptr || t.Kind() == reflect.Slice {
		t = t.Elem()
	}

	nestedAttributes := map[string]*pb.Plugin_SchemaAttribute{}
	// Add nested attributes if the field is a struct
	if t.Kind() == reflect.Struct {
		for i := 0; i < t.NumField(); i++ {
			attr, err := schemaAttributeFromField(t.Field(i))
			if err != nil {
				return nil, fmt.Errorf("failed to parse schema field for nested attribute %s: %w", t.Field(i).Name, err)
			}
			nestedAttributes[attr.Name] = attr
		}

		// If all the subfields are optional, then the parent is optional as well.
		var hasRequired bool

		for _, sf := range nestedAttributes {
			if sf.Required != nil && *sf.Required {
				hasRequired = true
			}
		}

		if !hasRequired {
			required = false
		}
	}

	// Parse doc tag
	desc, deprecated := parseDocTag(f.Tag)

	return &pb.Plugin_SchemaAttribute{
		Name:             name,
		Type:             typeBuf,
		Description:      desc,
		Required:         &required,
		Deprecated:       deprecated,
		NestedAttributes: nestedAttributes,
	}, nil
}

func schemaBlockSpecFromStructField(f reflect.StructField) (*pb.Plugin_SchemaBlockSpec, error) {
	t := f.Type

	// Check if block is optional
	optional := t.Kind() == reflect.Ptr

	// Unwind pointers
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	// Determine the block type
	var cType cty.Type
	switch t.Kind() {
	case reflect.Struct:
		cType = cty.EmptyObject
	case reflect.Slice:
		cType = cty.List(cty.EmptyObject)
	case reflect.Map:
		cType = cty.Map(cty.EmptyObject)
	default:
		return nil, fmt.Errorf("invalid type for hcl block, must be struct, slice, or map")
	}

	// Unwind container type
	for t.Kind() == reflect.Ptr || t.Kind() == reflect.Slice || t.Kind() == reflect.Map {
		t = t.Elem()
	}

	typeBuf, err := cType.MarshalJSON()
	if err != nil {
		return nil, err
	}

	// Get the schema block for the nested struct
	block, err := schemaBlockFromStruct(reflect.New(t).Interface())
	if err != nil {
		return nil, err
	}

	// Parse doc tag
	desc, deprecated := parseDocTag(f.Tag)
	// Parse hcl tag
	name, _, _ := parseHclTag(f.Tag)

	response := &pb.Plugin_SchemaBlockSpec{
		Name:        name,
		Description: desc,
		Deprecated:  deprecated,
		Type:        typeBuf,
		Required:    !optional,
		Block:       block,
	}

	return response, nil
}

func schemaBlockFromStruct(v interface{}) (*pb.Plugin_SchemaBlock, error) {
	response := &pb.Plugin_SchemaBlock{
		Attributes: map[string]*pb.Plugin_SchemaAttribute{},
		BlockSpecs: map[string]*pb.Plugin_SchemaBlockSpec{},
	}

	rv := reflect.ValueOf(v)

	// Unwind pointers
	if rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	if rv.Kind() != reflect.Struct {
		return nil, fmt.Errorf("invalid block type %s, must be struct", rv.Kind().String())
	}

	t := rv.Type()

	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)

		// HCL tag is required
		_, ok := f.Tag.Lookup("hcl")
		if !ok {
			return nil, fmt.Errorf("missing hcl tag on field: %s", f.Name)
		}

		// Parse hcl tag
		name, _, isBlock := parseHclTag(f.Tag)

		// Check if this field is a block or an attribute
		if isBlock {
			block, err := schemaBlockSpecFromStructField(f)
			if err != nil {
				return nil, err
			}
			response.BlockSpecs[name] = block
		} else {
			attr, err := schemaAttributeFromField(f)
			if err != nil {
				return nil, err
			}
			response.Attributes[name] = attr
		}
	}

	return response, nil
}

func schemaAttributesFromActionOutput(v interface{}) (map[string]*pb.Plugin_SchemaAttribute, error) {
	response := map[string]*pb.Plugin_SchemaAttribute{}

	t := reflect.TypeOf(v)
	// Unwind pointers, slices, and maps
	for t.Kind() == reflect.Ptr || t.Kind() == reflect.Slice || t.Kind() == reflect.Map {
		t = t.Elem()
	}

	if t.Kind() != reflect.Struct {
		return response, nil
	}

	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)

		name := f.Tag.Get("cty")
		if name == "" {
			name = strcase.ToSnake(f.Name)
		}

		// Get implied cty type
		ty, err := gocty.ImpliedType(reflect.New(f.Type).Interface())
		if err != nil {
			return nil, fmt.Errorf("failed to get implied cty type for field %s: %w", f.Name, err)
		}
		typeBuf, err := ty.MarshalJSON()
		if err != nil {
			return nil, err
		}

		// Parse doc tag
		desc, deprecated := parseDocTag(f.Tag)

		attr := &pb.Plugin_SchemaAttribute{
			Name:        name,
			Description: desc,
			Deprecated:  deprecated,
			Type:        typeBuf,
		}

		nestedAttributes, err := schemaAttributesFromActionOutput(reflect.New(f.Type).Interface())
		if err != nil {
			return nil, err
		}
		attr.NestedAttributes = nestedAttributes

		response[name] = attr
	}

	return response, nil
}

func parseHclTag(tag reflect.StructTag) (string, bool, bool) {
	val := tag.Get("hcl")
	parts := strings.Split(val, ",")

	required := true
	for _, p := range parts[1:] {
		if p == "optional" {
			required = false
		}
	}

	block := false
	for _, p := range parts[1:] {
		if p == "block" {
			block = true
		}
	}

	return parts[0], required, block
}

func parseDocTag(tag reflect.StructTag) (string, bool) {
	val := tag.Get("doc")
	parts := strings.Split(val, ",")
	if len(parts) > 1 && parts[1] == "deprecated" {
		return parts[0], true
	}
	return val, false
}
