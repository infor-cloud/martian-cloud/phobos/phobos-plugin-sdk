package plugin

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"sync"

	"github.com/hashicorp/go-argmapper"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/protostructure"
	"github.com/iancoleman/strcase"
	"github.com/zclconf/go-cty/cty"
	"github.com/zclconf/go-cty/cty/gocty"
	ctyjson "github.com/zclconf/go-cty/cty/json"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	empty "google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal/funcspec"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal/pluginargs"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/proto/gen"
)

// ActionExecutionResponse is the response returned after executing an action
type ActionExecutionResponse struct {
	outputData map[string]cty.Value
}

// Outputs returns the output data from the action
func (c *ActionExecutionResponse) Outputs() map[string]cty.Value { return c.outputData }

var (
	_                   component.PipelinePluginActionResponse = (*ActionExecutionResponse)(nil)
	actionInputDataType                                        = reflect.TypeOf((*pipeline.ActionInput)(nil)).Elem()
)

// PipelinePlugin implements plugin.Plugin (specifically GRPCPlugin) for
// the pipeline component type.
type PipelinePlugin struct {
	plugin.NetRPCUnsupportedPlugin
	Impl    pipeline.Plugin
	Logger  hclog.Logger
	Mappers []*argmapper.Func
}

// GRPCServer returns the GRPC server
func (p *PipelinePlugin) GRPCServer(broker *plugin.GRPCBroker, s *grpc.Server) error {
	base := &base{
		Mappers: p.Mappers,
		Logger:  p.Logger,
		Broker:  broker,
	}

	pb.RegisterPipelineServiceServer(s, &pipelineServer{
		base: base,
		Impl: p.Impl,
	})
	return nil
}

// GRPCClient returns the GRPC client
func (p *PipelinePlugin) GRPCClient(
	_ context.Context,
	broker *plugin.GRPCBroker,
	c *grpc.ClientConn,
) (interface{}, error) {
	client := &pipelineClient{
		client:  pb.NewPipelineServiceClient(c),
		logger:  p.Logger,
		broker:  broker,
		mappers: p.Mappers,
	}

	return client, nil
}

type pipelineClient struct {
	client  pb.PipelineServiceClient
	logger  hclog.Logger
	broker  *plugin.GRPCBroker
	mappers []*argmapper.Func
}

func (c *pipelineClient) Schema(ctx context.Context) (*component.PipelinePluginSchema, error) {
	// Get the schema
	resp, err := c.client.Schema(ctx, &empty.Empty{})
	if err != nil {
		return nil, fmt.Errorf("failed to get schema from pipeline: %w", err)
	}

	actionSchemas := map[string]*component.PipelinePluginActionSchema{}
	for name, a := range resp.Actions {
		input, err := schemaBlockFromProto(a.Input)
		if err != nil {
			return nil, fmt.Errorf("failed to convert schema block proto for action %s: %w", name, err)
		}

		outputs := map[string]*component.SchemaAttribute{}
		for _, o := range a.Outputs {
			attr, err := schemaAttributeFromProto(o)
			if err != nil {
				return nil, fmt.Errorf("failed to convert schema attribute proto for action %s and output %s: %w", name, o.Name, err)
			}
			outputs[attr.Name] = attr
		}

		actionSchemas[name] = &component.PipelinePluginActionSchema{
			Name:        name,
			Description: a.Description,
			Group:       a.Group,
			Input:       input,
			Outputs:     outputs,
		}
	}

	pipelineBlock, err := schemaBlockFromProto(resp.Config)
	if err != nil {
		return nil, fmt.Errorf("failed to convert schema block proto: %w", err)
	}

	return &component.PipelinePluginSchema{
		Config:  pipelineBlock,
		Actions: actionSchemas,
	}, nil
}

func (c *pipelineClient) Shutdown(ctx context.Context) error {
	_, err := c.client.Shutdown(ctx, &empty.Empty{})
	return err
}

func (c *pipelineClient) Config(ctx context.Context) (interface{}, error) {
	resp, err := c.client.ConfigStruct(ctx, &empty.Empty{})
	if err != nil {
		return nil, err
	}

	if resp.Struct == nil {
		return nil, nil
	}

	result, err := protostructure.New(resp.Struct)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (c *pipelineClient) ConfigSet(ctx context.Context, v interface{}) error {
	jsonv, err := json.Marshal(v)
	if err != nil {
		return err
	}

	_, err = c.client.Configure(ctx, &pb.Config_ConfigureRequest{
		Json: jsonv,
	})
	return err
}

func (c *pipelineClient) ActionInput(ctx context.Context, actionName string) (interface{}, error) {
	resp, err := c.client.ActionInputStruct(ctx, &pb.Pipeline_ActionInputStructRequest{ActionName: actionName})
	if err != nil {
		return nil, err
	}

	result, err := protostructure.New(resp.Struct)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (c *pipelineClient) ActionOutput(ctx context.Context, actionName string) (interface{}, error) {
	resp, err := c.client.ActionOutputStruct(ctx, &pb.Pipeline_ActionOutputStructRequest{ActionName: actionName})
	if err != nil {
		return nil, err
	}

	result, err := protostructure.New(resp.Struct)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (c *pipelineClient) ExecuteAction(ctx context.Context, input *component.PipelinePluginActionInput, evalCtx *hcl.EvalContext) interface{} {
	// Get the spec
	spec, err := c.client.ExecuteActionSpec(ctx, &pb.Pipeline_ExecuteActionSpecRequest{
		ActionName: input.ActionName,
	})
	if err != nil {
		return funcErr(err)
	}

	return funcspec.Func(spec, c.executeActionFunc(input, evalCtx),
		argmapper.Logger(c.logger),
		argmapper.Typed(&pluginargs.Internal{
			Broker:  c.broker,
			Mappers: c.mappers,
			Cleanup: &pluginargs.Cleanup{},
		}),
	)
}

func (c *pipelineClient) executeActionFunc(input *component.PipelinePluginActionInput, evalCtx *hcl.EvalContext) interface{} {
	return func(
		ctx context.Context,
		args funcspec.Args,
	) (component.PipelinePluginActionResponse, error) {
		structResp, err := c.client.ActionInputStruct(ctx, &pb.Pipeline_ActionInputStructRequest{ActionName: input.ActionName})
		if err != nil {
			return nil, err
		}

		result, err := protostructure.New(structResp.Struct)
		if err != nil {
			return nil, err
		}

		// Decode
		if diag := gohcl.DecodeBody(input.InputData, evalCtx, result); len(diag) > 0 {
			return nil, diag
		}

		jsonv, err := json.Marshal(result)
		if err != nil {
			return nil, err
		}
		// Call our function
		resp, err := c.client.ExecuteAction(ctx, &pb.Pipeline_ExecuteActionRequest{
			ActionName: input.ActionName,
			Json:       jsonv,
			Args:       &pb.FuncSpec_Args{Args: args},
		})
		if err != nil {
			return nil, err
		}

		outputType, err := ctyjson.UnmarshalType(resp.OutputType)
		if err != nil {
			return nil, err
		}

		outputVal, err := ctyjson.Unmarshal(resp.OutputValue, outputType)
		if err != nil {
			return nil, err
		}

		return &ActionExecutionResponse{
			outputData: outputVal.AsValueMap(),
		}, nil
	}
}

// pipelineServer is a gRPC server that the client talks to and calls a
// real implementation of the component.
type pipelineServer struct {
	*base

	pb.UnsafePipelineServiceServer // to avoid having to copy stubs into here for authServer

	Impl pipeline.Plugin

	pluginData interface{}

	cancelFuncs   []context.CancelFunc
	cancelFuncsMu sync.Mutex
}

func (s *pipelineServer) buildActionContext(ctx context.Context) context.Context {
	actionCtx, cancel := context.WithCancel(ctx)
	s.cancelFuncsMu.Lock()
	defer s.cancelFuncsMu.Unlock()
	s.cancelFuncs = append(s.cancelFuncs, cancel)
	return actionCtx
}

func (s *pipelineServer) Shutdown(_ context.Context, _ *empty.Empty) (*empty.Empty, error) {
	s.cancelFuncsMu.Lock()
	defer s.cancelFuncsMu.Unlock()
	for _, cancel := range s.cancelFuncs {
		cancel()
	}
	s.cancelFuncs = nil

	return &empty.Empty{}, nil
}

func (s *pipelineServer) ConfigStruct(
	_ context.Context,
	_ *empty.Empty,
) (*pb.Config_StructResp, error) {
	v, err := s.Impl.Config()
	if err != nil {
		return nil, err
	}

	encoded, err := protostructure.Encode(v)
	if err != nil {
		return nil, err
	}

	return &pb.Config_StructResp{Struct: encoded}, nil
}

func (s *pipelineServer) Configure(
	_ context.Context,
	req *pb.Config_ConfigureRequest,
) (*empty.Empty, error) {
	// Get our value that we can decode into
	v, err := s.Impl.Config()
	if err != nil {
		return nil, err
	}

	// Decode our JSON value directly into our structure.
	if err := json.Unmarshal(req.Json, v); err != nil {
		return nil, err
	}

	if err := s.Impl.ValidateConfig(v); err != nil {
		return nil, err
	}

	pluginData, err := s.Impl.PluginData(s.Logger)
	if err != nil {
		return nil, err
	}

	s.pluginData = pluginData

	return &empty.Empty{}, nil
}

func (s *pipelineServer) ActionInputStruct(
	_ context.Context,
	request *pb.Pipeline_ActionInputStructRequest,
) (*pb.Config_StructResp, error) {
	action, err := s.getAction(request.ActionName)
	if err != nil {
		return nil, err
	}

	f := action.ExecuteFunc()

	inputData, err := getActionInputStruct(f)
	if err != nil {
		return nil, err
	}

	encodedStruct, err := protostructure.Encode(inputData)
	if err != nil {
		return nil, err
	}

	return &pb.Config_StructResp{Struct: encodedStruct}, nil
}

func (s *pipelineServer) ActionOutputStruct(
	_ context.Context,
	request *pb.Pipeline_ActionOutputStructRequest,
) (*pb.Config_StructResp, error) {
	out, err := s.getActionOutputStructType(request.ActionName)
	if err != nil {
		return nil, err
	}

	encodedStruct, err := protostructure.Encode(out)
	if err != nil {
		return nil, err
	}

	return &pb.Config_StructResp{Struct: encodedStruct}, nil
}

func (s *pipelineServer) ExecuteActionSpec(
	ctx context.Context,
	request *pb.Pipeline_ExecuteActionSpecRequest,
) (*pb.FuncSpec, error) {
	if s.Impl == nil {
		return nil, status.Errorf(codes.Unimplemented, "plugin does not implement: action executor")
	}

	action, err := s.getAction(request.ActionName)
	if err != nil {
		return nil, err
	}

	spec, err := funcspec.Spec(action.ExecuteFunc(),
		argmapper.Logger(s.Logger),
		argmapper.ConverterFunc(s.Mappers...),
		argmapper.Typed(s.internal(ctx)),
	)

	if err != nil {
		return nil, status.Errorf(codes.Unimplemented, "spec failed with error")
	}

	return spec, err
}

func (s *pipelineServer) ExecuteAction(
	ctx context.Context,
	request *pb.Pipeline_ExecuteActionRequest,
) (*pb.Pipeline_ExecuteActionResponse, error) {
	internal := s.internal(ctx)
	defer internal.Cleanup.Close()

	s.Logger.Info("Executing action %s", request.ActionName)

	action, err := s.getAction(request.ActionName)
	if err != nil {
		return nil, err
	}

	if c, ok := action.(pipeline.ConfigurableAction); ok {
		if err := c.Configure(s.pluginData, s.Logger); err != nil {
			return nil, err
		}
	}

	f := action.ExecuteFunc()

	inputData, err := getActionInputStruct(f)
	if err != nil {
		return nil, err
	}

	// Decode our JSON value directly into our structure.
	if err := json.Unmarshal(request.Json, inputData); err != nil {
		return nil, err
	}

	actionCtx := s.buildActionContext(ctx)

	raw, err := callDynamicFunc(f, request.Args.Args,
		argmapper.ConverterFunc(s.Mappers...),
		argmapper.Logger(s.Logger),
		argmapper.Typed(actionCtx),
		argmapper.Typed(inputData),
		argmapper.Typed(internal),
	)
	if err != nil {
		return nil, err
	}

	result := &pb.Pipeline_ExecuteActionResponse{}

	outputMap, err := convertStructToOutputMap(raw)
	if err != nil {
		return nil, err
	}

	ctyVal := cty.ObjectVal(outputMap)
	ctyType := ctyVal.Type()

	encodedValue, err := ctyjson.Marshal(ctyVal, ctyType)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal action output cty value: %w", err)
	}

	encodedType, err := ctyjson.MarshalType(ctyType)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal action output cty type: %w", err)
	}

	result.OutputValue = encodedValue
	result.OutputType = encodedType

	return result, nil
}

func (s *pipelineServer) Schema(
	_ context.Context,
	_ *emptypb.Empty,
) (*pb.Pipeline_SchemaResponse, error) {
	// Get pipeline config
	c, err := s.Impl.Config()
	if err != nil {
		return nil, err
	}

	pipelineInputBlock, err := schemaBlockFromStruct(c)
	if err != nil {
		return nil, err
	}

	actionSchemas := map[string]*pb.Pipeline_ActionSchema{}
	for _, f := range s.Impl.GetActions() {
		action := f()
		meta := action.Metadata()
		if meta == nil {
			return nil, fmt.Errorf("action metadata not found, this is an issue with the plugin")
		}

		inputStruct, err := getActionInputStruct(action.ExecuteFunc())
		if err != nil {
			return nil, err
		}

		actionInputBlock, err := schemaBlockFromStruct(inputStruct)
		if err != nil {
			return nil, err
		}

		actionSchema := &pb.Pipeline_ActionSchema{
			Description: meta.Description,
			Group:       meta.Group,
			Input:       actionInputBlock,
		}

		outStructType, err := s.getActionOutputStructType(meta.Name)
		if err != nil {
			return nil, err
		}

		if outStructType != nil {
			outputStruct := reflect.New(outStructType).Interface()
			outputFields, err := schemaAttributesFromActionOutput(outputStruct)
			if err != nil {
				return nil, err
			}

			actionSchema.Outputs = outputFields
		}

		actionSchemas[meta.Name] = actionSchema
	}

	return &pb.Pipeline_SchemaResponse{
		Config:  pipelineInputBlock,
		Actions: actionSchemas,
	}, nil
}

func (s *pipelineServer) getAction(name string) (pipeline.Action, error) {
	for _, f := range s.Impl.GetActions() {
		action := f()
		meta := action.Metadata()
		if meta == nil {
			return nil, fmt.Errorf("action %s has nil metadata", name)
		}
		if meta.Name == name {
			return action, nil
		}
	}
	return nil, fmt.Errorf("action %s not found", name)
}

func (s *pipelineServer) getActionOutputStructType(actionName string) (reflect.Type, error) {
	action, err := s.getAction(actionName)
	if err != nil {
		return nil, err
	}

	f := action.ExecuteFunc()

	v := reflect.ValueOf(f)
	if !v.IsValid() || v.Kind() != reflect.Func {
		return nil, fmt.Errorf("must be a function type, got %s", v.Type())
	}

	t := v.Type()

	// Return nil if the function has no output
	if t.NumOut() <= 0 {
		return nil, nil
	}

	out := t.Out(0)
	for {
		// Unwrap output type if it's a pointer
		if out.Kind() == reflect.Ptr {
			out = out.Elem()
			continue
		}

		break
	}

	if out.Kind() != reflect.Struct {
		return nil, nil
	}

	return out, nil
}

func getActionInputStruct(f interface{}) (interface{}, error) {
	fv := reflect.ValueOf(f)
	ft := fv.Type()
	if k := ft.Kind(); k != reflect.Func {
		return nil, fmt.Errorf("fn should be a function, got %s", k)
	}

	for i := 0; i < ft.NumIn(); i++ {
		argType := ft.In(i)

		if argType.Implements(actionInputDataType) {
			ptr := reflect.New(argType)
			ptr.Elem().Set(reflect.New(ptr.Elem().Type().Elem()))

			return ptr.Elem().Interface(), nil
		}
	}
	return nil, nil
}

func convertStructToOutputMap(v interface{}) (map[string]cty.Value, error) {
	result := map[string]cty.Value{}

	srcT := reflect.TypeOf(v)

	for srcT.Kind() == reflect.Ptr {
		srcT = srcT.Elem()
	}

	if srcT.Kind() != reflect.Struct {
		return nil, errors.New("action output type must be a struct")
	}

	val := reflect.ValueOf(v)
	for val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	for i := 0; i < srcT.NumField(); i++ {
		sf := srcT.Field(i)
		if sf.PkgPath != "" {
			// ignore unexported fields
			continue
		}

		if strings.HasPrefix(sf.Name, "XXX") {
			// ignore proto internals
			continue
		}

		ty, err := gocty.ImpliedType(val.Field(i).Interface())
		if err != nil {
			return nil, err
		}
		ctyVal, err := gocty.ToCtyValue(val.Field(i).Interface(), ty)
		if err != nil {
			return nil, err
		}

		name := strcase.ToSnake(sf.Name)
		result[name] = ctyVal
	}

	return result, nil
}

var (
	_ plugin.Plugin            = (*PipelinePlugin)(nil)
	_ plugin.GRPCPlugin        = (*PipelinePlugin)(nil)
	_ pb.PipelineServiceServer = (*pipelineServer)(nil)
	_ component.PipelinePlugin = (*pipelineClient)(nil)
)
