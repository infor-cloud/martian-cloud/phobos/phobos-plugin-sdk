package cmd

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/pluginclient"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/schema"
)

const schemaFormatVersion = "1.0"

type schemaCmd struct {
	commonCmd

	flagPluginDir string
}

func (cmd *schemaCmd) Synopsis() string {
	return "generates a schema json file for the plugin"
}

func (cmd *schemaCmd) Help() string {
	return "schema generates a schema json file for the plugin"
}

func (cmd *schemaCmd) Flags() *flag.FlagSet {
	fs := flag.NewFlagSet("schema", flag.ExitOnError)
	fs.StringVar(&cmd.flagPluginDir, "plugin-dir", "", "relative or absolute path to plugin directory")
	return fs
}

func (cmd *schemaCmd) Run(args []string) int {
	fs := cmd.Flags()
	err := fs.Parse(args)
	if err != nil {
		cmd.ui.Error(fmt.Sprintf("unable to parse flags: %s", err))
		return 1
	}

	return cmd.run(cmd.runInternal)
}

func (cmd *schemaCmd) launchPlugin(ctx context.Context, pluginDir string, buildDir string) (*plugin.Client, error) {
	pluginName := filepath.Base(pluginDir)

	outFile := filepath.Join(buildDir, pluginName)
	switch runtime.GOOS {
	case "windows":
		outFile = outFile + ".exe"
	}

	cmd.ui.Info(fmt.Sprintf("building plugin: %s", pluginDir))

	buildCmd := exec.Command("go", "build", "-o", outFile)
	buildCmd.Dir = pluginDir

	output, err := buildCmd.CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("error building plugin %q: %w: %s", buildCmd.Path, err, string(output))
	}

	cmd.ui.Info("Successfully built plugin")

	pluginLogger := hclog.New(&hclog.LoggerOptions{
		Name:        "phobos-plugin-sdk",
		Level:       hclog.LevelFromString("ERROR"),
		Output:      os.Stdout,
		Color:       hclog.AutoColor,
		DisableTime: true,

		IndependentLevels: true,
	})

	pluginCmd := exec.CommandContext(ctx, outFile) // nosemgrep: gosec.G204-1

	// We have to copy the command because go-plugin will set some
	// fields on it.
	cmdCopy := *pluginCmd

	config := pluginclient.ClientConfig(pluginLogger)
	config.Cmd = &cmdCopy
	config.Logger = pluginLogger

	// Stderr contains the logs
	config.Stderr = io.Discard
	// SynStderr is the stderr for the plugin process
	config.SyncStderr = io.Discard
	// SynStdout is the stdout for the plugin process
	config.SyncStdout = io.Discard

	// Connect to the plugin
	client := plugin.NewClient(config)

	return client, nil
}

func (cmd *schemaCmd) runInternal() error {
	ctx := context.Background()

	pluginDir := cmd.flagPluginDir
	if pluginDir == "" {
		dir, err := os.Getwd()
		if err != nil {
			return fmt.Errorf("error getting working directory: %w", err)
		}
		pluginDir = dir
	}

	tmpDir, err := os.MkdirTemp("", "phobos-plugin-sdk")
	if err != nil {
		return fmt.Errorf("unable to create temporary plugin install directory %q: %w", tmpDir, err)
	}
	defer os.RemoveAll(tmpDir)

	pluginShortName := strings.TrimPrefix(filepath.Base(pluginDir), "phobos-plugin-")

	client, err := cmd.launchPlugin(ctx, pluginDir, tmpDir)
	if err != nil {
		return fmt.Errorf("failed to launch plugin: %w", err)
	}
	defer client.Kill()

	rpcClient, err := client.Client()
	if err != nil {
		return err
	}

	cmd.ui.Info("generating plugin schema...")

	// Dispense the pipeline plugin
	rawPipelinePlugin, err := rpcClient.Dispense(strings.ToLower(component.PipelineType.String()))
	if err != nil {
		return fmt.Errorf("failed to dispense the pipeline plugin type: %w", err)
	}

	// Get the pipeline plugin schema
	pipelineSchema, err := rawPipelinePlugin.(component.PipelinePlugin).Schema(ctx)
	if err != nil {
		return fmt.Errorf("failed to get the pipeline plugin schema: %w", err)
	}

	// Dispense the metadata plugin
	rawMetadataPlugin, err := rpcClient.Dispense(strings.ToLower(component.MetadataType.String()))
	if err != nil {
		return fmt.Errorf("failed to dispense the metadata plugin type: %w", err)
	}

	// Get plugin metadata
	meta, err := rawMetadataPlugin.(component.MetadataPlugin).PluginMetadata(ctx)
	if err != nil {
		return fmt.Errorf("failed to get plugin metadata: %w", err)
	}

	pluginSchema := &schema.PluginSchema{
		Name:             pluginShortName,
		Description:      meta.Description,
		PipelineSchema:   pipelineSchema,
		ProtocolVersions: []string{"1.0"},
		FormatVersion:    schemaFormatVersion,
	}

	buf, err := json.MarshalIndent(pluginSchema, "", "    ")
	if err != nil {
		return fmt.Errorf("failed to marshal plugin schema: %w", err)
	}

	cmd.ui.Info("writing plugin schema to schema.json")

	if err := os.WriteFile(filepath.Join(pluginDir, "schema.json"), buf, 0600); err != nil {
		return fmt.Errorf("failed to write schema to schema.json: %w", err)
	}

	cmd.ui.Info("finished generating schema")

	return nil
}
