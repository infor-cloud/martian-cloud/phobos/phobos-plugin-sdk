// Package cmd provides the internal implementation for the CLI tool.
package cmd

import (
	"fmt"
	"os"

	"github.com/hashicorp/cli"
	"github.com/mattn/go-colorable"
)

type commonCmd struct {
	ui cli.Ui
}

func (cmd *commonCmd) run(r func() error) int {
	err := r()
	if err != nil {
		cmd.ui.Error(fmt.Sprintf("error executing command: %s\n", err))
		os.Exit(1)
	}
	return 0
}

// Run is the entrypoint for the CLI tool.
func Run(version string) int {
	ui := &cli.ColoredUi{
		ErrorColor: cli.UiColorRed,
		WarnColor:  cli.UiColorYellow,
		InfoColor:  cli.UiColorMagenta,

		Ui: &cli.BasicUi{
			Reader:      os.Stdin,
			Writer:      colorable.NewColorableStdout(),
			ErrorWriter: colorable.NewColorableStderr(),
		},
	}

	c := cli.NewCLI("phobosplugintools", version)
	c.Args = os.Args[1:]
	c.Commands = map[string]cli.CommandFactory{
		"schema": func() (cli.Command, error) {
			return &schemaCmd{
				commonCmd: commonCmd{
					ui: ui,
				},
			}, nil
		},
	}

	exitStatus, err := c.Run()
	if err != nil {
		return 1
	}

	return exitStatus
}
