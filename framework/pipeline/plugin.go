// Package pipeline is intended to be used by plugin developers to create new pipeline plugins
package pipeline

import "github.com/hashicorp/go-hclog"

//go:generate mockery --all --case="underscore"

// ActionInput is an argument type that is used by plugins as a vehicle for accepting
// action input data. A struct implementing this interface indicates to the plugin system
// that the struct should not be included in a grpc advertised dynamic function spec, because
// it will be injected on the plugin side, not supplied from core over GRPC.
type ActionInput interface {
	IsActionInput()
}

// ConfigurableAction is an action that can accept pipeline data
type ConfigurableAction interface {
	Configure(data interface{}, logger hclog.Logger) error
}

// ActionMetadata includes the action name and description which is used for documentation
type ActionMetadata struct {
	Name        string
	Description string
	Group       string
}

// Action represents an action that the pipeline plugin supports
type Action interface {
	Metadata() *ActionMetadata
	ExecuteFunc() interface{}
}

// Metadata includes the pipeline name and description which is used for documentation
type Metadata struct {
	Name        string
	Description string
}

// Plugin is a type of plugin which provides actions and data sources
type Plugin interface {
	Config() (any, error)
	ValidateConfig(config any) error
	PluginData(logger hclog.Logger) (any, error)
	GetActions() []func() Action
}
