// Code generated by mockery v2.20.0. DO NOT EDIT.

package mocks

import (
	hclog "github.com/hashicorp/go-hclog"
	mock "github.com/stretchr/testify/mock"
)

// ConfigurableAction is an autogenerated mock type for the ConfigurableAction type
type ConfigurableAction struct {
	mock.Mock
}

// Configure provides a mock function with given fields: data, logger
func (_m *ConfigurableAction) Configure(data interface{}, logger hclog.Logger) error {
	ret := _m.Called(data, logger)

	var r0 error
	if rf, ok := ret.Get(0).(func(interface{}, hclog.Logger) error); ok {
		r0 = rf(data, logger)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

type mockConstructorTestingTNewConfigurableAction interface {
	mock.TestingT
	Cleanup(func())
}

// NewConfigurableAction creates a new instance of ConfigurableAction. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewConfigurableAction(t mockConstructorTestingTNewConfigurableAction) *ConfigurableAction {
	mock := &ConfigurableAction{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
